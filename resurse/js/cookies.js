function setCookie(nume, val, timpExpirare) {//timpExpirare in milisecunde
    d = new Date();
    d.setTime(d.getTime() + timpExpirare);
    document.cookie = `${nume}=${JSON.stringify(val)};path=/;SameSite=Strict;Secure=true;expires=${d.toUTCString()};`
    // document.cookie = `${nume}=${val}; expires=${d.toUTCString()}`;
}

function getCookie(nume){
    vectorParametri = document.cookie.split(";"); 
    for (let param of vectorParametri) {
        if (param.trim().startsWith(nume + "=")) {
            return param.split("=")[1];
        }    
    }
    return null;
}

function deleteCookie(nume) {
    console.log(`${nume}; expires=${(new Date()).toUTCString()}`);
    document.cookie = `${nume}=0; expires=${(new Date()).toUTCString()}`;
}

function deleteAllCookies() {
    vectorParametri = document.cookie.split(";"); 
    for (let param of vectorParametri) {
        let numeCookie = param.split("=")[0];
        deleteCookie(numeCookie);
    }
}

window.addEventListener("load", function () {
    let banner = document.getElementById("banner");
    banner.classList.add("animatie_banner");

    if (getCookie("acceptat_banner")) {
        banner.style.display = "none";
    }

    this.document.getElementById("ok_cookies").onclick = function () {
        setCookie("acceptat_banner", true, 24 * 60 * 60 * 1000);
        banner.style.display = "none";
    }

    let produsElement = document.getElementById("art-produs");
    if (produsElement) {
        // if (!getCookie('ultimulProdusVizualizat')) {
        //     document.getElementById("ultimul-produs-vizualizat").style.display = "none";
        // }
        let timp = 60 * 60 * 60 * 1000;
        let produs = encodeURIComponent(document.getElementById("nume-prod-prod").innerHTML); // evitam trunchierea numelui din cauza unui caracter special (spre ex., &)  
        let idProd = document.getElementById("id-prod-prod").innerHTML.trim();
        setCookie('ultimulProdusVizualizat', produs, timp);
        setCookie('idUltProdViz', parseInt(idProd), timp);
 
    }

    let ultimulProdusVizualizat = getCookie('ultimulProdusVizualizat');
    let elem = document.getElementById("ultimul-produs-vizualizat")
    
    if (ultimulProdusVizualizat && elem) {
        let id = getCookie("idUltProdViz");
        let nume = decodeURIComponent(getCookie("ultimulProdusVizualizat"));

        let nod1 = this.document.createElement('h2');
        nod1.innerHTML = "Ultimul produs vizualizat";
        elem.appendChild(nod1);
        let nod2 = this.document.createElement('a');
        nod2.setAttribute('href', `/produs-1/${id}`);
        elem.appendChild(nod2);
        let nod3 = this.document.createElement('span');
        nod3.innerHTML = `${nume}`;
        nod2.appendChild(nod3);

        // let elementProdNou = `<h2>Ultimul produs vizualizat</h2>                           
        // <a href="/produs-1/${id}" >
        //     <span id="nume-prod-idx">${nume}
        //     </span>
        // </a>`;
        // elem.setHTMLUnsafe(elementProdNou);

        elem.style.display = "block";
    }
})
console.log(document.cookie)

document.addEventListener("DOMContentLoaded", function () {
    const galerie = document.getElementById("container-ga");
    const nrImag = genereazaNumarRandom();

    fetch("resurse/json/galerie-animata.json")
        .then(response => response.json())
        .then(data => {
            let imags = data.imagini.filter((_, index) => index % 2 == 0).slice(0, nrImag);
            imags.forEach((imag, index) => {
                let imagElem = document.createElement('img');
                imagElem.src = data.cale_galerie + "/" + imag.cale_fisier;
                imagElem.className = "item-ga";
                imagElem.style.zIndex = nrImag - index;
                galerie.appendChild(imagElem);
            })
            startAnimation();
        });

        function genereazaNumarRandom() {
            // putere a lui doi intre 1 si 17
            // let puterileLuiDoi = [2, 4, 8, 16];
            let puterileLuiDoi = [2, 4, 8];
            // random = [0...3] 
            let random = Math.floor(Math.random() * puterileLuiDoi.length);
            return puterileLuiDoi[random];
        }
        
        function startAnimation() {
            let currentIndex = 0;
            const items = document.querySelectorAll('.item-ga');
            intervalId = setInterval(() => {
                // for (let i = 0; i < length + 1; i++){
                //     if (i == currentIndex) {
                //         items[i].style.animation = 'clipPathAnimation 5s infinite';
                //     } else {
                //         items[i].style.animation = 'none';
                //     }
                // }


                items.forEach((item, index) => {
                    if (index === currentIndex) {
                        // item.style.transition = 'clip-path 2s ease-out';
                        item.style.animation = 'clipPathAnimation 5s infinite';
                    } else {
                        // item.classList.remove('animation');
                        item.style.animation = 'none';
                        // item.style.transition = 'none';
                    }
                });
                currentIndex = (currentIndex + 1) % items.length;
            }, 5000); // 5 seconds for each transition
        } 
});


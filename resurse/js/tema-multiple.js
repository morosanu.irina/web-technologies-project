/*--------------------multiple theme select----------------------*/

window.addEventListener("DOMContentLoaded", function () {
    loadTema();
    let selector = document.getElementById("selector-tema");
    selector.addEventListener("change", function () {
        setTema(selector.value);
    });  
});

function setTema(tm) {
    //resetam tema la tema light
    document.body.className = '';
    if (tm != 'light') {
        document.body.classList.add(tm);
        localStorage.setItem("tema-mult", tm);
    } else {
        localStorage.removeItem("tema-mult");
    }

}

function loadTema() {
    if (document.body.classList.contains("darkdark") ) {
        document.body.classList.remove("darkdark");
        localStorage.removeItem("tema-mult");
    } else if (document.body.classList.contains("contrast")) {
        document.body.classList.remove("contrast");
        localStorage.removeItem("tema-mult");
    } else {
        var temaSalvata = localStorage.getItem("tema-mult");
        if (temaSalvata) {
            document.body.classList.add(temaSalvata);
            document.getElementById("selector-tema").value = temaSalvata;
        } else {
            document.body.classList = '';
            document.getElementById("selector-tema").value = 'light';
        }
    }

    
}





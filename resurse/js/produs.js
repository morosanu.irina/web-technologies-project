window.addEventListener("DOMContentLoaded", afisareData);

// formatare data: 'zi-nume_luna-an [zi_saptamana]', de exemplu, 15-Septembrie-2018 [Sambata]
function afisareData() {
    let data_prod_string = document.getElementById("data-adaug-prod").innerHTML;
    let data_prod = new Date(data_prod_string);
    let data_formatata_prod = formateazaData(data_prod);
    document.getElementById("data-adaug-prod").innerHTML = data_formatata_prod;
}

function formateazaData(data) {
let formatDataRo = new Intl.DateTimeFormat('ro-RO', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' });
let dataFormatata = formatDataRo.format(data);
let partiData = dataFormatata.split(' '); // ex.: joi, 23 mai 2024 => ['joi','23','mai','2024']
let zi = partiData[1]; 
let luna = partiData[2];
let an = partiData[3];
let ziSaptamana = partiData[0].split(',')[0]; // split(','): 'joi,' => ['joi',''];
let ziSaptCapitaliz = ziSaptamana.replace(ziSaptamana[0], ziSaptamana[0].toUpperCase());

return `${zi}-${luna}-${an} [${ziSaptCapitaliz}]`;
}



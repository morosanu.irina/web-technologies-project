
/*----------------------dark/light theme only-----------------------*/

window.addEventListener("DOMContentLoaded", function () {
    if (localStorage.getItem("tema")) {
        document.body.classList.add("dark");
        document.getElementById("icon-tema").classList.add("bi-moon-fill");
        document.getElementById("icon-tema").classList.remove("bi-sun");
        document.getElementById("switch-tema").checked = true;
    }
    
    this.document.getElementById("icon-tema").onclick = function () {
        if (document.body.classList.contains("dark")) {
            document.body.classList.remove("dark");
            localStorage.removeItem("tema");
            document.getElementById("icon-tema").classList.remove("bi-moon-fill");
            document.getElementById("icon-tema").classList.add("bi-sun");
            document.getElementById("switch-tema").checked = false;
        } else {
            document.body.classList.add("dark");
            localStorage.setItem("tema", "dark");
            document.getElementById("icon-tema").classList.add("bi-moon-fill");
            document.getElementById("icon-tema").classList.remove("bi-sun");
            document.getElementById("switch-tema").checked = true;
        }
    }
})

window.addEventListener("DOMContentLoaded", function () {
    this.document.getElementById("switch-tema").onclick = function () {
        if (document.body.classList.contains("dark")) {
            document.body.classList.remove("dark");
            localStorage.removeItem("tema");
            document.getElementById("icon-tema").classList.remove("bi-moon-fill");
            document.getElementById("icon-tema").classList.add("bi-sun");
            document.getElementById("switch-tema").checked = false;
        } else {
            document.body.classList.add("dark");
            localStorage.setItem("tema", "dark");
            document.getElementById("icon-tema").classList.add("bi-moon-fill");
            document.getElementById("icon-tema").classList.remove("bi-sun");
            document.getElementById("switch-tema").checked = true;
        }
    }
})


window.addEventListener("DOMContentLoaded", function () {
    var produse = document.getElementsByClassName("produs");
    // afisare animata a produselor
    for (let i = 0; i < produse.length; i++) {
        setTimeout(function () {
            produse[i].style.display = "grid";
        }, (i + 1) * 100);
        afisareData(produse[i]);
    }
    capitalizeazaLabel();

    document.getElementById("inp-njuc").onchange = function () {
        document.getElementById("jucRange").innerHTML = `(${this.value})`;
        aplicaFiltre();
    }
    document.getElementById("inp-varsta").onchange = function () {
        document.getElementById("val-varsta-afisata").innerHTML = `(${this.value})`;
        aplicaFiltre();
    }

    document.getElementById("inp-nume").onchange = aplicaFiltre;
    document.getElementById("inp-brand").onchange = aplicaFiltre;
    document.getElementById("inp-limba").onchange = aplicaFiltre;
    document.getElementById("inp-tematica").onchange = aplicaFiltre;
    document.getElementById("inp-story").onchange = aplicaFiltre;
    document.getElementById("inp-categ-mult").onchange = aplicaFiltre;
    document.querySelectorAll(".tmec-chbx").forEach(chbx => chbx.onchange = aplicaFiltre);
    document.querySelectorAll("[name='gr_rad']").forEach(rb => rb.onchange = aplicaFiltre);
    document.querySelectorAll("[name='inp-extensie']").forEach(rb => rb.onchange = aplicaFiltre);

    //facem disable la butonul de filtrare, caci nu il folosim (l-am fi putut scoate de tot)
    this.document.getElementById("filtrare").setAttribute('disabled', true);

    function aplicaFiltre() {
        let val_nume = document.getElementById("inp-nume").value.toLowerCase().trim();
        let val_nume_normalizat = normalizeazaText(val_nume);
        let radiobuttons = document.getElementsByName("gr_rad");
        let val_pret, pret_lim_inf, pret_lim_sup;
        for (let r of radiobuttons) {
            if (r.checked) {
                val_pret = r.value;
                break;
            }
        }
        if (val_pret != "toate") {
            [pret_lim_inf, pret_lim_sup] = val_pret.split(":");
            pret_lim_inf = parseFloat(pret_lim_inf);
            pret_lim_sup = parseFloat(pret_lim_sup);
            // console.log("pret intre "+pret_lim_inf+" "+pret_lim_sup)
        }
        let njuc = parseInt(document.getElementById("inp-njuc").value);
        let varsta_select = parseInt(document.getElementById("inp-varsta").value);
        let brand_select = document.getElementById("inp-brand").value.toLowerCase();
        let brand_select_normalizat = normalizeazaText(brand_select);
        let limba_select = document.getElementById("inp-limba").value.trim().toLowerCase();
        var opt_multi_select = document.getElementById('inp-categ-mult').selectedOptions;
        var val_multi_select = Array.from(opt_multi_select).map(({ value }) => value);
        let mecanism_select = document.querySelectorAll('.tmec-chbx:checked');
        let mecanism_select_val = [];
        for (let e of mecanism_select) {
            mecanism_select_val.push(e.value);
        }
        let opt_extensie = document.getElementsByName("inp-extensie");
        let extensie_select = "";
        for (let o of opt_extensie) {
            if (o.checked) {
                extensie_select = o.value;
            } 
        }
        let descriere_select = document.getElementById("inp-story").value.toLowerCase().trim();
        let descriere_select_normalizata = normalizeazaText(descriere_select);
        if (!descriere_select_normalizata.match(new RegExp("^[a-z]*$"))) {
            alert("Introduceti doar litere in descriere!");
            document.getElementById("inp-story").classList.add("is-invalid");
            return;
        }
        document.getElementById("inp-story").classList.remove("is-invalid");
        let tematica_select = document.getElementById("inp-tematica").value.toLowerCase().trim();
        let tematica_select_normalizat = normalizeazaText(tematica_select);
        // console.log(tematica_select_normalizat);
        
        let count = 0;

        for (let prod of produse) {
            
            let nume = prod.getElementsByClassName("val-nume")[0].innerHTML.toLowerCase().trim();
            let nume_normalizat = normalizeazaText(nume);
            // let cond_nume = (nume.includes(val_nume));
            let cond_nume = (nume_normalizat.includes(val_nume_normalizat));
            
            let pret_produs = parseFloat(prod.getElementsByClassName("val-pret")[0].innerHTML);
            let cond_pret = (val_pret == "toate" || pret_lim_inf <= pret_produs && pret_produs <= pret_lim_sup);
            
            let nrMaxJuc = parseInt(prod.getElementsByClassName("max-juc")[0].innerHTML);
            let nrMinJuc = parseInt(prod.getElementsByClassName("min-juc")[0].innerHTML);
            let cond_juc_1 = (njuc == 0) || (njuc >= nrMinJuc && njuc <= nrMaxJuc);
            let cond_juc_2 = false;
            if (!nrMinJuc || !nrMaxJuc) {
                cond_juc_2 = true;
            }

            let varsta = parseInt(prod.getElementsByClassName("val-varsta")[0].innerHTML);
            let cond_varsta_1 = (varsta_select == 0) || (varsta >= varsta_select);
            let cond_varsta_2 = false;
            if (!varsta) {
                cond_varsta_2 = true;
            }
            
            let brand = prod.getElementsByClassName("val-brand")[0].innerHTML.toLowerCase().trim();
            let brand_normalizat = normalizeazaText(brand);
            // let cond_brand = brand.includes(brand_select);
            let cond_brand = brand_normalizat.includes(brand_select_normalizat);

            let lb = prod.getElementsByClassName("val-lb")[0].innerHTML.toLowerCase().trim();
            let cond_lb = (limba_select == "oricare" || limba_select == lb);
  
            let tematica = prod.getElementsByClassName("val-tema")[0].innerHTML;
            let tema;
            let tema_normalizata = new Array();
            let cond_tema = true;
            let cond_tema_inp_text = (tematica_select_normalizat=="");
            // in the case if the field "tematica" contains values
            if (tematica) {
                tema = tematica.trim().split(",");
                for (let val of val_multi_select) {
                    if (tema.includes(val)) {
                        cond_tema = false;
                    }
                }
                //normalizam textul din lista de la tematica (categorie)
                for (let t of tema) {
                    tema_normalizata.push(normalizeazaText(t));
                }
                if (tema_normalizata.includes(tematica_select_normalizat)) {
                    cond_tema_inp_text = true;
                }
            }  

            let mec = prod.getElementsByClassName("val-mecanism")[0].innerHTML;
            // let cond_mec = (mecanism_select_val.length == 0) || mecanism_select_val.includes(mec);
            let cond_mec = (mecanism_select_val.length==8) || mecanism_select_val.includes(mec);

            let extensie = prod.getElementsByClassName("val-extens")[0].innerHTML.toLowerCase().trim();
            if (extensie == "da") {
                extensie = "true";
            } else if (extensie == "nu") {
                extensie = "false";
            }
            // let cond_extens = (extensie_select == "") || (extensie == extensie_select);
            let cond_extens = true;
            let categorie = prod.getElementsByClassName("val-categorie")[0].innerHTML.toLowerCase();
            if (extensie_select == "") {
                cond_extens = true;
            } else if (categorie == "boardgames" || categorie == "colectionabile") {
                cond_extens = (extensie == extensie_select);
            } else {
                cond_extens = false;
            }
            
            let descriere = prod.getElementsByClassName("val-descr")[0].innerHTML.toLowerCase().trim();
            let descriere_normalizata = normalizeazaText(descriere);
            let text = document.getElementById("inp-story").innerHTML.toLowerCase().trim();
            let text_normalizat = normalizeazaText(text);
            // let cond_descr = (descriere_select == "") || (descriere_select == text) || (descriere.includes(descriere_select));
            let cond_descr = (descriere_select_normalizata == "") || (descriere_select_normalizata == text_normalizat) || (descriere_normalizata.includes(descriere_select_normalizata));

            // console.log("nume produs " + nume);
            // console.log("c nume " + cond_nume);
            // console.log("c pret " + cond_pret);
            // console.log("c juc " + cond_juc_1 + " || " + cond_juc_2);
            // console.log("c varsta " + cond_varsta_1 + " || " + cond_varsta_2);
            // console.log("c brand " + cond_brand);
            // console.log("c limba " + cond_lb);
            // console.log("c mecanism " + cond_mec);
            // console.log("c tema " + cond_tema);
            // console.log("c extensie " + cond_extens);
            // console.log("c descriere " + cond_descr);
            // console.log("c tematica " + cond_tema_inp_text);
            // console.log("---------------------------------------")
            
            if (cond_nume && cond_pret && (cond_juc_1 || cond_juc_2) && (cond_varsta_1 || cond_varsta_2) && cond_brand && cond_lb && cond_mec && cond_tema && cond_extens && cond_descr && cond_tema_inp_text) {
                prod.style.display = "grid";
                count++;
            } else {
                prod.style.display = "none";
            }
        }
        
        if (count == 0) {
            let p = document.getElementById("fara-produse");
            p.style.display = "block";
        }
    }

    // capitalizeaza prima litera a label-urilor
    function capitalizeazaLabel() {
        let elemClasaLabel = document.getElementsByClassName("titlu-filtre");
        for (let i = 0; i < elemClasaLabel.length; i++) {
            let label = elemClasaLabel[i].innerHTML;
            let labelCapitalizat = label.replace(label[0], label[0].toUpperCase());
            document.getElementsByClassName("titlu-filtre")[i].innerHTML = labelCapitalizat;
        }
    }


    // formatare data: 'zi-nume_luna-an [zi_saptamana]', de exemplu, 15-Septembrie-2018 [Sambata]
    function afisareData(produs) {
            let data_prod_string = produs.getElementsByClassName("val-data")[0].innerHTML;
            let data_prod = new Date(data_prod_string);
            let data_formatata_prod = formateazaData(data_prod);
            produs.getElementsByClassName("val-data")[0].innerHTML = data_formatata_prod;
    }

    function formateazaData(data) {
        let formatDataRo = new Intl.DateTimeFormat('ro-RO', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' });
        let dataFormatata = formatDataRo.format(data);
        let partiData = dataFormatata.split(' '); // ex.: joi, 23 mai 2024 => ['joi','23','mai','2024']
        let zi = partiData[1]; 
        let luna = partiData[2];
        let an = partiData[3];
        let ziSaptamana = partiData[0].split(',')[0]; // split(','): 'joi,' => ['joi',''];
        let ziSaptCapitaliz = ziSaptamana.replace(ziSaptamana[0], ziSaptamana[0].toUpperCase());
        
        return `${zi}-${luna}-${an} [${ziSaptCapitaliz}]`;
    }

    function normalizeazaText(text) {
        return text.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase();
    }


    document.getElementById("resetare").onclick = function () {
        let confirmReset = confirm("Sunteti sigur ca doriti sa resetati filtrele?");
        if (confirmReset) {
            document.getElementById("inp-nume").value = "";
            document.getElementById("inp-tematica").value = "";
            document.getElementById("i_rad7").checked = true;
            document.getElementById("inp-njuc").value = document.getElementById("inp-njuc").min;
            document.getElementById("jucRange").innerHTML = "(0)";
            document.getElementById("inp-varsta").value = document.getElementById("inp-varsta").min;
            document.getElementById("val-varsta-afisata").innerHTML = "(0)";
            document.getElementById("inp-brand").value = "";
            document.getElementById("inp-limba").value = "oricare";
            document.getElementById('inp-categ-mult').selectedIndex = -1;
            let check_mec = document.getElementsByClassName("tmec-chbx");
            for (let i = 0; i < check_mec.length; i++){
                check_mec[i].checked = true;
            }
            let rad_extensie = document.getElementsByName("inp-extensie");
            for (let i = 0; i < rad_extensie.length; i++) {
                rad_extensie[i].checked = false;            
            }
            document.getElementById("inp-story").value = "";     
            
            var produse = document.getElementsByClassName("produs");
            var v_produse = Array.from(produse);
            v_produse.sort(function (a, b) {
                let str_id_a = a.getElementsByClassName("val-id")[0].innerHTML;
                let id_a = str_id_a.replace("ar_ent_", '');
                let str_id_b = b.getElementsByClassName("val-id")[0].innerHTML;
                let id_b = str_id_b.replace("ar_ent_", '');
                return id_a - id_b;
            });
            for (let prod of v_produse) {
                prod.parentElement.appendChild(prod);
                prod.style.display = "grid";
            }
            document.getElementById("fara-produse").style.display = "none";
        }              
    }

    /*----------sortare multiple chei------------*/
    this.document.getElementById("sortCrescPret").setAttribute("disabled", true);
    this.document.getElementById("sortDescrescPret").setAttribute("disabled", true);

    document.getElementById("apply-sort").onclick = function () {
        let primarySort = document.getElementById("primary-sort").value;
        let secondarySort = document.getElementById("secondary-sort").value;
        if (primarySort === secondarySort) {
            alert("Alegeti chei de sortare diferite!");
            return;
        }
        let order = document.getElementById("inp-ordine-sort").value;
        let semn = (order === "asc") ? 1 : -1;
        sortare(semn, primarySort, secondarySort);
    }
    
    function sortare(semn, primarySort, secondarySort) {
        var produse = document.getElementsByClassName("produs");
        var v_produse = Array.from(produse);
    
        v_produse.sort(function (a, b) {
            let primaryA = getValue(a, primarySort);
            let primaryB = getValue(b, primarySort);
            if (primaryA === primaryB) {
                let secondaryA = getValue(a, secondarySort);
                let secondaryB = getValue(b, secondarySort);
                return compareValues(secondaryA, secondaryB, semn);
            }
            return compareValues(primaryA, primaryB, semn);
        });
    
        for (let prod of v_produse) {
            prod.parentElement.appendChild(prod);
        }
    }
    
    function getValue(element, key) {
        switch (key) {
            case "nume":
                return element.getElementsByClassName("val-nume")[0].innerHTML.toLowerCase();
            case "pret":
                return parseFloat(element.getElementsByClassName("val-pret")[0].innerHTML);
            case "mecanism":
                return element.getElementsByClassName("val-mecanism")[0].innerHTML.toLowerCase();
            default:
                return "";
        }
    }
    
    function compareValues(valueA, valueB, semn) {
        if (typeof valueA === "string" && typeof valueB === "string") {
            return semn * valueA.localeCompare(valueB);
        }
        return semn * (valueA - valueB);
    }


    document.getElementById("btn-suma").onclick = function () {
        
        if (document.getElementById("suma-calculata")) {
            return;
        }
            
        var produse = document.getElementsByClassName("produs");
        let suma = 0;
        for (let prod of produse) {
            let eSelectat = prod.getElementsByClassName("select-cos")[0].checked;
            if (eSelectat) {
                let pret = parseFloat(prod.getElementsByClassName("val-pret")[0].innerHTML);
                suma += pret;
            }  
        }
        let d = document.createElement("div");
        d.innerHTML = "pret total: " + suma + " lei";
        d.id = "suma-calculata";
        let ds = document.getElementById("calcSuma");
        let container = ds.parentNode;
        let frate = ds.nextElementSibling;
        container.insertBefore(d, frate);
        setTimeout(function () {
            let info = document.getElementById("suma-calculata");
            if (info)
                info.remove();
        }, 2000)
    }
    
    document.getElementById("btn-sterge-sel").onclick = function () {
        let check_select = document.getElementsByClassName("select-cos");
        for (let i = 0; i < check_select.length; i++){
            check_select[i].checked = false;
        } 
    }

});
DROP TABLE IF EXISTS produse;
DROP TYPE IF EXISTS categ_produs;
DROP TYPE IF EXISTS tip_mecanism;


CREATE TYPE categ_produs AS ENUM( 'Boardgames', 'Puzzle', 'Colectionabile', 'Accesorii');
CREATE TYPE tip_mecanism AS ENUM('tabla modulara', 'plasare muncitori', 'contracte', 'aruncare zaruri', 'management al mainii de joc', 'puzzle', 'echilibru', 'altele');

CREATE TABLE IF NOT EXISTS produse (
   id serial PRIMARY KEY,
   nume VARCHAR(100) UNIQUE NOT NULL,
   descriere TEXT,
   pret NUMERIC(8,2) NOT NULL CHECK (pret>0),
   an_aparitie INT,
   producator VARCHAR(50),  
   limba VARCHAR(10),    
   categorie categ_produs DEFAULT 'Boardgames',
   nr_min_jucatori INT CHECK (nr_min_jucatori>0),
   nr_max_jucatori INT CHECK (nr_min_jucatori>0),
   varsta_min INT CHECK (nr_min_jucatori>0),
   mecanism tip_mecanism DEFAULT 'tabla modulara',
   nr_piese INT CHECK (nr_min_jucatori>0),
   tematica VARCHAR [],
   expansiune BOOLEAN DEFAULT FALSE,
   imagine VARCHAR(300),
   data_adaugare TIMESTAMP DEFAULT current_timestamp
);



INSERT into produse (nume, descriere, pret, an_aparitie, producator, limba, categorie, nr_min_jucatori, nr_max_jucatori, varsta_min, mecanism, nr_piese, tematica, expansiune, imagine, data_adaugare) VALUES
('Five Tribes', 'Creat de Bruno Cathala, jocul Five Tribes este pe stilul jocurilor germane care au miniaturi din lemn (meeples). În acest joc este aplicată o variantă a clasicului worker placement și anume că jocul incepe cu muncitorii deja plasați pe hartă, iar jucătorii trebuie să îi mute inteligent pe hartă (sate, piețe, oaze și locuri sacre). Cum, când și unde plasezi aceste cinci triburi: Assasins, Elders, Builders, Merchants și Viziers va determina victoria sau eșecul. ', 269,	2014, 'Days of Wonder', 'Engleza',
'Boardgames', 2, 4, 13, 'tabla modulara', 327, '{"arab", "fantastic", "mitologie"}', False, 'five-tribes.jpg', '2024-04-08 12:05:06'),

('Lords of Waterdeep', 'Waterdeep, the City of Splendors - cel mai frumos oras din Forgotten Realms si totodata un cuib al intrigilor politice si a deal-urilor in aleiile intunecate. Jucatorii iau rolul unor puternici lorzi care vor sa conduca acest oras. Comorile si resursele orasului sunt a celor ce o sa le ia primii iar ce nu se poate lua prin negociere si inselaciune trebuie luate cu forta. ', 240, 2012, 'Wizards of the Coast', 'Engleza', 'Boardgames', 2, 5, 12, 'plasare muncitori', 432, '{"fantastic", "construire orase"}', False, 'lords-waterdeep.jpg', '2024-04-08 12:05:06'),

('Istanbul', 'In jocul Istanbul, conduci un grup format din 4 asistenti si 1 negustor prin 16 locatii dintr-un bazar. La fiecare locatie poti face o actiune specifică. Pentru a putea face o acțiune trebuie să muți negustorul si un asistent acolo iar apoi sa lasi asistentul in spate pentru a se ocupa de detalii în timp ce tu te ocupi de lucruri mai importante. Daca vrei sa folosesti acel asistent mai târziu, negustorul trebuie să se întoarcă în acea locație pentru a-l lua de acolo. Astfel trebuie sa iti planifici foarte bine mutările pentru a nu rămâne fără asistenți. ', 155, 2014, 'Lex Games', 'Romana', 'Boardgames', 2, 5, 10, 'contracte', 254, '{"economic"}', False, 'istanbul.jpg', '2024-04-08 12:05:06'),

('7 Wonders Duel'	, 'Jocul 7 Wonders Duel se aseamna foarte mult cu jocul original. Diferenta este ca e special pentru doi jucatori, cartile nu vor fi draftate, ci luate din mijlocul mesei. Jocul poate fi castigat in 3 moduri: Militar cucerind capitala adversarului, prin stiinta daca strangi 6 sau 7 simboluri sau avand cele mai multe puncte la final.', 124, 2015, 'Repos Production', 'Romana', 'Boardgames', 2, 2, 10, 
'tabla modulara', 135, '{"antichitate", "carti", "construire orase", "civilizatie", "economic"}', False, '7-wonders-duel.jpg', '2024-04-08 12:05:06'),

('Century Spice Road', 'Century: Spice Road este primul dintr-o serie care exploreaza istoria fiecarui secol de comerț de mirodenii. 
In joc jucatorii sunt liderii unor caravane care traverseaza faimoasa cale a matasii pentru a face negot cu mirodenii pentru faima si glorie.
Editie germană. ', 115, 2017, 'Asmodee', 'Germana', 'Boardgames', 2, 5, 8, 'contracte', 224, '{"carti", "economic", "medieval"}', 
 False, 'century-road.jpg', '2024-04-08 12:05:06'),

('Champions of Midgard: Valhalla', 'In Valhalla, cea de-a doua expansiune pentru Champions of Midgard, vei fi rasplatit atunci cand războinicii tai vor muri in lupta. Vei primi obiecte, relicve si alti razboinici pentru a le putea utiliza in lupta in Midgard. Printre luptătorii noi adusi in jocul de baza sunt Berzerkers si Shieldwarriors, noi zaruri si puteri pentru lideri, care vor extinde si mai mult aventura in Midgard. ', 124, 2017, 'Grey Fox Games', 'Engleza', 'Boardgames', 2, 4, 10, 'plasare muncitori', 208, '{"zaruri","mitologie", "fantastic", "medieval"}', True, 
'midgard-valhalla.jpg','2024-04-08 12:05:06'),

('Ticket to ride: Europe', 'Ticket to Ride: Europe te poartă într-o nouă aventură cu trenul, prin toată Europa. De la Edinburgh la Constantinopol si de la Lisabona la Moscova, vei vizita marile orase ale Europei la trecerea dintre secolele 19 si 20. Jocul nu vine doar cu o noua harta ci si cu noi elemente de joc, cum ar fi Tunelele, Bacurile, Garile si cartile de joc mai mari. Ca si jocul original, jocul este elegant si simplu, se invata in 5 minute si se poate juca cu toata familia. Ticket to Ride: Europe este un joc nou și complet, nu este o expansiune. ', 238.2, 2005, 'Days of Wonder', 'Romana', 'Boardgames', 2, 5, 8, 'contracte', 420, '{"auto", "trenuri"}', False, 'ticket-ride-europe.jpg', '2024-04-08 12:05:06'),

('Lords of Waterdeep: Scoundrels of Skullport', 'Lords of Waterdeep: Scoundrels of Skullport adauga doua noi extensii pentru jocul Lords of Waterdeep - Undermountain si Skullport - inspirate din dungeon-ul si raiul criminalilor de sub Waterdeep. \nExtensia The Skullport adauga o noua resursa, Corruption, in timp ce extensia Undermountain are quest-uri mai mari si mai multe moduri de a aduna aventurieri. Se pot folosi impreuna sau separat. 
Include noi Lords, noi cladiri si materiale de joc necesare pentru al saselea jucator. ', 200, 2013, 'Wizards of the Coast', 'Engleza', 'Boardgames', 2, 6, 12, 'plasare muncitori', 208, '{"construire orase", "fantastic"}', True, 'lords-waterdeep-skullport.jpg', '2024-04-08 12:05:06'),

('King of Tokyo', 'In King of Tokyo te joci cu monștri mutanți, roboți gigantici si extraterestri care se luptă pentru a deveni Regele din Tokyo! ', 166, 2011, 'Lex Games', 'Romana', 'Boardgames', 2, 6, 8, 'aruncare zaruri', 166, '{"zaruri", "lupta", "carti"}', False, 'king-tokyo.jpg', '2024-04-11 09:00:00'),

('Kanagawa',  'Anul 1840. In Kanagawa, un golf din Tokyo, Maestrul Hokusai a decis sa deschida o scoala de arte pentru a impartasi cunostiintele sale.

Tu esti unul dintre discipolii sai si mai mult vrei neaparat sa arati ca esti cel mai bun. Respecta indicatiile sale si picteaza subiectele preferate (copaci, animale, personaje, cladiri) asta avand in vedere sezoanele care se schimba pentru ca ai nevoie de vremea perfecta pentru a scoate tabloul perfect.', 143, 2016, 'IELLO', 'Engleza', 'Boardgames', 2, 4, 10, 'plasare muncitori', 187, '{"animale", "carti"}', False, 'kanagawa.jpg', '2024-04-11 09:00:00'),

('Takenoko', 'Cu mult timp in urma la curtea imperiala Japoneza, imparatul chinez a facut cadou un panda imparatului japonez, in semn de pace. De atunci, imparatul japonez a avut incredere in membrii curtii (jucatorii) sa aiba grija de panda in a sa curte de bambus. 
In jocul Takenoko, jucatorii vor cultiva plantatii, le vor iriga si vor creste una dintre cele trei specii de bambus (verde, galben si roz) cu ajutorul gradinarului imperial. Acestia vor trebui sa hraneasca si sa ingrijeasca ursul oferindui hrana preferata, bambusul. Jucatorul care isi administreaza plantatiile de bambus cel mai bine si va face fericit ursul panda, va castiga. ', 152, 2011, 'Lex Games', 'Romana', 'Boardgames', 2, 4, 8, 'tabla modulara', 197, '{"animale", "miniaturi"}', False, 'takenoko.jpg', '2024-04-11 09:00:00'),
 
('Llamas Unleashed', 'Llamas Unleashed este un joc Unstable Unicorns in care scopul este sa aduci 7 animale pe campul tau, inaintea adversarilor. 
Llamas Unleashed este un joc standalone care nu necesita Unstable Unicorns pentru a fi jucat si include 135 de carta intr-o cutie magnetica. Are aceleasi mecanici de baza de joc, dar cu mici twist-uri. ', 99, 2019, 'Unstable Games', 'Engleza', 'Boardgames', 2, 8, 14, 'management al mainii de joc', 136, 
 '{"animale", "carti", "petrecere"}', False, 'llama.jpg', '2024-04-11 09:00:00'),

('Disney Lorcana: The First Chapter', 'Disney Lorcana este un joc de trading card (TCG) cu personaje din universul Disney, având grafică atât originală dar și reimaginată într-o lume noiă cu stil de joc unic și magic. Disney Lorcana nu are o limită oficială de jucători, dar designerii au sugerat că 2-6 jucători este intervalul optim.', 99, 2023, 'Ravensburger', 'Engleza', 'Colectionabile', 2, 6, 8, 'management al mainii de joc', 86, '{"carti", "colectionabile"}', 
 False, 'disney-lorcana.jpg', '2024-04-22 19:30:58'),

('Disney Lorcana: Into the Inklands', 'Pornește într-o călătorie epică pentru a-ți construi pachetul suprem! Strânge cărți rare și provoca-i pe ceilalți entuziaști Disney Lorcana la lupta.\nDeveniți o legendă Lorcana!', 211, 2024, 'Ravensburger', 'Engleza', 'Colectionabile', 2, 6, 8, 'management al mainii de joc',
 86, '{"carti", "colectionabile"}', True, 'disney-lorcana-inklands.jpg', '2024-04-22 19:30:58'),

('Pokémon TCG: Sun & Moon Expansion', 'Sun & Moon este o expansiune în limba engleză care corespunde aproximativ expansiunii contemporane în limba japoneză Collection Sun/Collection Moon. ', 78, 2017, 'The Pokémon Company International', 'Engleza', 'Colectionabile', 2, 2, 6, 'management al mainii de joc', 70, 
'{"carti", "colectionabile"}', True, 'pokemon-sun-moon.jpg', '2024-04-22 19:30:58'),

('Jenga', 'Contine 54 de piese de lemn, care trebuie construite ca un turn dupa care fiecare jucator pe rand trage cate o bucata de lemn de jos si o pune deasupra. Primul la care sa darama turnul pierde.
Un joc rapid, distractiv, rejucabil, super compact in cutia cilindrica.', 93.5, 1983, 'Hasbro', 'Engleza', 'Boardgames', 1, 8, 6, 'echilibru', 56, '{"dexteritate", "petrecere"}', False, 'jenga.jpg', '2024-04-22 19:45:02');


INSERT into produse (nume, descriere, pret, producator, categorie, mecanism, nr_piese, tematica, imagine, data_adaugare) VALUES
('Ultimate Guard Precise-Fit Sleeves Standard Size Transparent (100)', 'Pachet cu 100 de folii pentru a fi folosite ca protectie 
 interioara pentru o protectie superioara. Dimensiune standard pentru jocuri ca Magic The Gathering, Pokemon si altele.
 - se potrivesc perfect in Ultimate Guard Sleeves Standard Size (sleeve in sleeve)
 - potrivire precisa pe carte\n- marime consistenta
 - polipropilena de 
 inalta claritate
 - fara acid, fara PVC
 - dimensiune: 64x89 mm
 - 50 microni. ', 15, 'heo GmbH', 'Accesorii', 'altele', 100, '{"carti", "colectionabile"}', 
 'ultimate-guard-sleeves-100.jpg', '2024-04-22 15:44:15'),

('Turn Zaruri', 'Pachetul conține:
- turn de zaruri - dimensiuni(mm):~120x63x67
- tavița - dimensiuni(mm):~132x77x28
- saculeț pentru depozitare/protectie bumbac
- 5 zaruri D6 (12mm)\nTurnul si tavița sunt produse manual din placaj de fag și finisate cu baiț si lac pe bază de apă.
Varietatea dispunerii fibrelor lemnului fac din fiecare turn un produs unic.', 110, 'heo GmbH', 'Accesorii', 'altele', 8, '{"zaruri"}', 'turn-zar.jpg', '2024-05-10 15:44:15');


INSERT into produse (nume, descriere, pret, producator, categorie, mecanism, tematica, imagine, data_adaugare) VALUES
('Organizator 7 WONDERS DUEL', 'Organizer pentru jocul 7 Wonders DUEL in care se pot stoca toate componentele in siguranta si are un compartiment special pentru depozitare a monedelor. Sertarele se pot separa cu ajutorul a 4 despartitoare, include compartimente de depozitare a cartilor si exista spatiu si pentru urmatoarele expansiuni.
Dimensiuni: 195mm x 195mm x 44.7mm.
Acest produs este un kit si necesita asamblare cu lipici special pentru lemn (nu este inclus).', 86, 'The Broken Token', 'Accesorii', 'altele', '{"antichitate", "carti", "construire orase", "civilizatie", "economic"}', 'organizer-7-wonders-duel.jpg', '2024-05-10 15:44:15');


INSERT into produse (nume, descriere, pret, producator, categorie, nr_min_jucatori, varsta_min, mecanism, nr_piese, tematica, imagine, data_adaugare) VALUES
('Puzzle 3D Lemn Retro Car Eco Wood Art', 'Dimensiune 30 x 17 x 15.50 cm.', 222, 'EWA Eco-Wood-Art', 'Puzzle', 1, 9, 'puzzle', 315, '{"lemn", "3d", "auto"}', 'puzzle-3d-motocicleta.jpg', '2024-05-10 15:44:15'),

('Puzzle 3D Lemn Triceratops Eco Wood Art', 'Dimensiune 32 x 16 x 10.50 cm.', 174, 'EWA Eco-Wood-Art', 'Puzzle', 1, 9, 'puzzle', 283, '{"lemn","3d", "animale"}', 'puzzle-3d-dinozaur.jpg', '2024-05-10 15:44:15');


INSERT into produse (nume, descriere, pret, producator, categorie, nr_min_jucatori, varsta_min, mecanism, nr_piese, tematica, imagine) VALUES
('Puzzle 1000 piese Euphoric Spectrum', 'Puzzle-ul are dimensiunile 68 x 47 cm si cutia 35 x 25 x 5 cm.', 52.5, 'Castorland', 'Puzzle', 1, 9, 'puzzle', 1000, '{"carton", "jigsaw", "animale"}', 'puzzle-caine-castorland.jpg'),

('Puzzle 3000 piese Tiger Sanctuary', 'Cutia are dimensiunile de 38×26.5×5 cm, iar puzzle-ul are 92×68 cm. ', 99.4, 'Castorland', 'Puzzle', 1, 9, 'puzzle', 3000, '{"carton", "jigsaw", "animale"}', 'puzzle-tigru-castorland.jpg'),

('Puzzle 2000 piese Train Station', 'Dimensiune 68.8 x 96.6 cm. Autor Christoph Schöne.', 138, 'Heye', 'Puzzle', 1, 9, 'puzzle', 2000, '{"carton", "jigsaw", "trenuri", "auto"}', 'puzzle-tren-heye.jpg'),

('Puzzle 2000 piese Deep Jungle', 'Dimensiune 68.8 x 96.6 cm. Autor Michael Ryba.', 138, 'Heye', 'Puzzle', 1, 9, 'puzzle', 2000, '{"carton", "jigsaw", "animale"}', 'puzzle-jungla-heye.jpg');


CREATE USER admin_wt_project WITH ENCRYPTED PASSWORD 'admin';
GRANT ALL PRIVILEGES ON DATABASE wt_project TO admin_wt_project ;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO admin_wt_project;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO admin_wt_project;
const express = require("express"); //incarcarea in program a pachetului express a lui Node.js; returneaza o functie
const fs = require('fs'); //fs - file system 
const path = require('path');
const sharp = require('sharp');
const sass = require('sass');
const ejs = require('ejs');
const { Client } = require('pg');

var client = new Client(
    {
        database: "wt_project",
        user: "admin_wt_project",
        password: "admin",
        host: "localhost",
        port: 5432
    }
);
client.connect();

app = express();    //cream serverul = obiectul app (aplicatie)
console.log("Folder proiect", __dirname);
console.log("Cale fisier", __filename);
console.log("Director de lucru", process.cwd());

obGlobal = {
    obErori: null,
    obImagini: null,
    obToateImagGalAnim: null,
    obImagGalAnim: null,
    folderScss: path.join(__dirname, "resurse/scss"),
    folderCss: path.join(__dirname, "resurse/css"),
    folderBackup: path.join(__dirname, "backup"),
    optiuniMeniu: null,
   
}

vect_foldere = ["temp", "backup"]
for (let folder of vect_foldere) {
    let caleFolder = path.join(__dirname, folder);
    if (!fs.existsSync(caleFolder)) {
        fs.mkdirSync(caleFolder);
    }
}

app.set("view engine", "ejs");

app.use("/resurse", express.static(__dirname + "/resurse"));
app.use("/node_modules", express.static(__dirname + "/node_modules"));

client.query("select * from unnest(enum_range(null::categ_produs))", function (err, rezCatProd) {
    if (err) {
        console.log(err);
    } else {
        obGlobal.optiuniMeniu = rezCatProd.rows;
    }
})


app.get(["/", "/home", "/index"], function (request, response) {
    var numarImagGalAnim = genereazaNumar();
    console.log("nr din index "+numarImagGalAnim)
    var temp = getImagesWithEvenIndex(numarImagGalAnim);
    obGlobal.obImagGalAnim = temp.slice();
    // obGlobal.obImagGalAnim = temp.map(elem => { return { ...elem } });
    // console.log("galeria animata: ");
    // console.log(obGlobal.obImagGalAnim);
    response.render("pagini/index", { ip: request.ip, imagini: obGlobal.obImagini.imagini, optiuniMeniu: obGlobal.optiuniMeniu, imagGalAnim: obGlobal.obImagGalAnim });
    console.log("obImagGalAnim din /index ")
    console.log(obGlobal.obImagGalAnim)
})

app.get("/compounded", function (request, response) {
    response.render("pagini/compounded")
})

app.get("/istoric", function (request, response) {
    response.render("pagini/istoric", { optiuniMeniu: obGlobal.optiuniMeniu })
})

app.get("/galerie", function (request, response) {
    response.render("pagini/galerie", { imagini: obGlobal.obImagini.imagini, optiuniMeniu: obGlobal.optiuniMeniu });
})

app.get("*/galerie-animata-try.css", function (req, res) {
    var sirScss = fs.readFileSync(path.join(__dirname, "resurse/scss/galerie_animata-try.scss")).toString("utf8");

    var numarImagGalAnim = obGlobal.obImagGalAnim.length;
    // rezScss = ejs.render(sirScss, { nrImg: numarImagGalAnim});
    // console.log(rezScss);
    const dynamicScss = `$nr-img: ${numarImagGalAnim};\n${sirScss}`;

    var caleScss = path.join(__dirname, "temp/galerie_animata-try.scss");
    // fs.writeFileSync(caleScss, rezScss);
    fs.writeFileSync(caleScss, dynamicScss);
    try {
        rezCompilare = sass.compile(caleScss, { sourceMap: true });
        
        var caleCss = path.join(__dirname, "temp/galerie_animata-try.css");
        fs.writeFileSync(caleCss, rezCompilare.css);
        res.setHeader("Content-Type", "text/css");
        res.sendFile(caleCss);
    }
    catch (err){
        console.log(err);
        res.send("Eroare");
    }
});

app.get("*/galerie-animata-try.css.map", function (req, res) {
    res.sendFile(path.join(__dirname, "temp/galerie-animata-try.css.map"));
});


app.get("/favicon.ico", function (request, response) {
    response.sendFile(path.join(__dirname, "resurse/favicon/favicon.ico"));
})


// client.query("select * from produse", function (err, rez) {
//     if (err) {
//         console.log(err);
//     } else {
//         // obGlobal.optiuniMeniu = rez.rows;
//         console.log(rez);
//     }
// })

// ---------------------------------- Produse ----------------------------------
app.get("/produse", function (req, res) {
    // console.log("tip=",req.query.tip)
    // client.query("select * from unnest(enum_range(null::categ_produs))", function (err, rezOptiuni) {
    //     if (err) {
    //         console.log(err);
    //     } else {
    //         let conditieWhere = "";
    //         if (req.query.tip) {
    //             conditieWhere = ` where mecanism='${req.query.tip}'`       //"where tip='"+req.query.tip+"'"
    //         }
    //         client.query("select * from produse " + conditieWhere, function (err, rez) {
    //             // console.log(300);
    //             if (err) {
    //                 console.log(err);
    //                 afisareEroare(res, 2);
    //             } else {
    //                 // console.log(rez);
    //                 console.log(optiuni);
    //                 res.render("pagini/produse", { produse: rez.rows, optiuni: rezOptiuni.rows });
    //             }
    //         });
    //     }
    // });


    var conditieQuery = "";
    if (req.query.cat) {
        conditieQuery = ` where categorie='${req.query.cat}'`;
    }


    client.query("select distinct(unnest(tematica)) from produse order by unnest asc", function (err, rezTematica) {
        if (err) {
            console.log(err);
        } else {
            
            client.query("select column_name from information_schema.columns where table_name = N'produse'", function (err, numeColoane) {
                // console.log("nume col: "+JSON.stringify(numeColoane.rows[0]))
                client.query("select distinct(unnest(enum_range(null::tip_mecanism))) from produse order by unnest asc", function (err, rezMecanism) {
                    // console.log("rezMec: ")
                    // console.log(rezMecanism.rows[1])
                    if (err) {
                        console.log(err);
                    } else {
                        if (err) {
                            console.log(err);
                        } else {
                            client.query("select max(nr_max_jucatori) as max_juc, min(varsta_min) as min_age, max(varsta_min) as max_age, min(pret) as min_pret, max(pret) as max_pret from produse", function (err, rezAtribute) {
                                // console.log(rezAtribute)
                                // console.log("label: "+rezAtribute.fields[0].name)

                                client.query("select distinct(limba) as limba from produse", function (err, rezLimba) {
                                    console.log(rezLimba)
                                    if (err) {
                                        console.log(err);
                                    } else {
                                        client.query(`select * from produse ${conditieQuery}`, function (err, rezAll) {
                                            if (err) {
                                                console.log(err);
                                            } else {
                                                if (err) {
                                                    console.log(err);
                                                    afisareEroare(res, 2);
                                                } else {
                                                    res.render("pagini/produse",
                                                        {
                                                            produse: rezAll.rows,
                                                            optiuniMeniu: obGlobal.optiuniMeniu,
                                                            optiuni: rezTematica.rows,
                                                            labelJuc: numeColoane.rows[1].column_name,
                                                            labelVarsta: numeColoane.rows[2].column_name,
                                                            labelMecanism: numeColoane.rows[3].column_name,
                                                            labelExpansiune: numeColoane.rows[5].column_name,
                                                            labelPret: numeColoane.rows[7].column_name,
                                                            labelCategorie: numeColoane.rows[9].column_name,
                                                            labelNume: numeColoane.rows[11].column_name,
                                                            labelDescriere: numeColoane.rows[12].column_name,
                                                            labelTematica: numeColoane.rows[13].column_name,
                                                            labelProducator: numeColoane.rows[15].column_name,
                                                            labelLimba: numeColoane.rows[16].column_name,
                                                            nrMaxJuc: rezAtribute.rows[0].max_juc,
                                                            valMinVarsta: rezAtribute.rows[0].min_age,
                                                            valMaxVarsta: rezAtribute.rows[0].max_age,
                                                            mecanisme: rezMecanism.rows,
                                                            limba: rezLimba.rows,
                                                            pretMin: rezAtribute.rows[0].min_pret,
                                                            pretMax: rezAtribute.rows[0].max_pret
                                                        })
                                                }
                                            }
                        
                                        })
                                    }
                                })

                                
                            })
                        }
                    }
                   
                    
                })



                
            })       
        }
 
    });


    // client.query("select * from unnest(enum_range(null::categ_produs))", function(err, rezOptiuni){
    //     client.query(`select * from produse ${conditieQuery}`, function(err, rez){
    //         if (err){
    //             console.log(err);
    //             afisareEroare(res, 2);
    //         }
    //         else{
    //             res.render("pagini/produse", {produse: rez.rows, optiuniMeniu: rezOptiuni.rows})
    //         }
    //     })
    // });

    // client.query("select distinct(unnest(tematica)) from produse order by unnest asc", function(err, rezOpt){
    //     client.query(`select * from produse ${conditieQuery}`, function(err, rez){
    //         if (err){
    //             console.log(err);
    //             afisareEroare(res, 2);
    //         }
    //         else{
    //             res.render("pagini/produse", {produse: rez.rows, optiuni: rezOpt.rows})
    //         }
    //     })
    // });
});

app.get("/produs/:id", function (req, res) {
    client.query(`select * from produse where id = ${req.params.id}`, function (err, rez) {
        if (err) {
            console.log(err);
            afisareEroare(res, 2);
        } else {   
            // console.log(rez)
            res.render("pagini/produs", { prod: rez.rows[0], optiuniMeniu: obGlobal.optiuniMeniu });
        }
    });

    // client.query("select * from unnest(enum_range(null::categ_produs))", function (err, rezCatProd) {
    //     if (err) {
    //         console.log(err);
    //     } else {
            
    //         client.query(`select * from produse where id=${req.params.id}`, function (err, rez) {
    //             if (rez.rowCount == 0 || err) {
    //                 console.log(err);
    //                 afisareEroare(res, 2);
    //             } else {
    //                 res.render("pagini/produs", { prod: rez.rows[0], optiuniMeniu: rezCatProd.rows });
    //             }
    //         });
    //     }
    // });
});


app.get("/*.ejs", function (request, response) {
    afisareEroare(response, 400);
})

app.get(new RegExp("^\/resurse\/[a-zA-Z0-9_\/-]+\/$"), function (request, response) {
    afisareEroare(response, 403);
});

app.get("/*", function (request, response) {
    console.log(request.url);
    try {
        response.render("pagini" + request.url, function (eroare, rezultatRandare) {
            console.log("Eroare: ", eroare);
            console.log("Html: ", rezultatRandare);
            if (eroare) {
                if (eroare.message.startsWith("Failed to lookup view")) {
                    afisareEroare(response, 404);
                    console.log("Nu a gasit pagina: ", request.url);
                }
            } else {
                response.send(rezultatRandare);
            }
            response.send(rezultatRandare);
        })
    } catch (err) {
        if (err.message.startsWith("Cannot find module")) {
            afisareEroare(response, 404);
            console.log("Nu a gasit resursa: ", request.url);
            return;
        }
        afisareEroare(response);
    }

})
function initErori() {
    let continut = fs.readFileSync(path.join(__dirname, "resurse/json/erori.json")).toString("utf-8");
    obGlobal.obErori = JSON.parse(continut);
    for (let eroare of obGlobal.obErori.info_erori) {
        eroare.imagine = path.join(obGlobal.obErori.cale_baza, eroare.imagine);
    }
    let err_def = obGlobal.obErori.eroare_default;
    err_def.imagine = path.join(obGlobal.obErori.cale_baza, err_def.imagine);
}
initErori()

function afisareEroare(response, _identificator, _titlu, _text, _imagine) {
    let eroare = obGlobal.obErori.info_erori.find(
        function (elem) {
            return elem.identificator == _identificator;
        }
    )
    if (!eroare)
        eroare = obGlobal.obErori.eroare_default;
    
    response.render("pagini/eroare",
        {
            titlu: _titlu || eroare.titlu,
            text: _text || eroare.text,
            imagine: _imagine || eroare.imagine,
            optiuniMeniu: obGlobal.optiuniMeniu
        }
    )
}

function initImagini() {
    var continut = fs.readFileSync(path.join(__dirname, "resurse/json/galerie.json")).toString("utf-8");

    obGlobal.obImagini = JSON.parse(continut);
    let vImagini = obGlobal.obImagini.imagini;

    let caleAbs = path.join(__dirname, obGlobal.obImagini.cale_galerie);
    let caleAbsMediu = path.join(__dirname, obGlobal.obImagini.cale_galerie, "mediu");
    let caleAbsMic = path.join(__dirname, obGlobal.obImagini.cale_galerie, "mic");
    if (!fs.existsSync(caleAbsMediu))
        fs.mkdirSync(caleAbsMediu);
    if (!fs.existsSync(caleAbsMic))
        fs.mkdirSync(caleAbsMic);

    for (let imag of vImagini) {
        // [numeFis, ext] = imag.fisier.split(".");
        [numeFis, ext] = splitLastOccurence(imag.cale_fisier, ".");
        let caleFisAbs = path.join(caleAbs, imag.cale_fisier);
        
        // galerie ecran mediu
        let caleFisMediuAbs = path.join(caleAbsMediu, numeFis + ".webp");
        sharp(caleFisAbs).resize(300).toFile(caleFisMediuAbs);
        imag.fisier_mediu = path.join("/", obGlobal.obImagini.cale_galerie, "mediu", numeFis + ".webp")
        
        // galerie ecran mic
        let caleFisMicAbs = path.join(caleAbsMic, numeFis + ".webp");
        sharp(caleFisAbs).resize(200).toFile(caleFisMicAbs);
        imag.fisier_mic = path.join("/", obGlobal.obImagini.cale_galerie, "mic", numeFis + ".webp")

        imag.fisier = path.join("/", obGlobal.obImagini.cale_galerie, imag.cale_fisier)
    }
    // console.log(obGlobal.obImagini)
}
initImagini();

function initImagGalAnim() {
    var continut = fs.readFileSync(path.join(__dirname, "resurse/json/galerie-animata.json")).toString("utf-8");
    obGlobal.obToateImagGalAnim = JSON.parse(continut);
    let vImagini = obGlobal.obToateImagGalAnim.imagini;

    for (let imag of vImagini) {
        [numeFis, ext] = splitLastOccurence(imag.cale_fisier, ".");
        imag.fisier = path.join("/", obGlobal.obToateImagGalAnim.cale_galerie, imag.cale_fisier);
    }
    // console.log(obGlobal.obToateImagGalAnim)
}
initImagGalAnim();

function splitLastOccurence(str, substr) {
    lastIndex = str.lastIndexOf(substr);
    before = str.slice(0, lastIndex);
    after = str.slice(lastIndex + 1);
    return [before, after];
}

function compileazaScss(caleScss, caleCss) {
    // console.log("cale: ", caleCss);
    if (!caleCss) {
        let numeFisExt = path.basename(caleScss);
        let numeFis = numeFisExt.split('.')[0];
        caleCss = numeFis + ".css";
    }

    if (!path.isAbsolute(caleScss)) {
        caleScss = path.join(obGlobal.folderScss, caleScss);
    }

    if (!path.isAbsolute(caleCss)) {
        caleCss = path.join(obGlobal.folderCss, caleCss);
    }

    let caleBackup = path.join(obGlobal.folderBackup, "resurse/css");
    if (!fs.existsSync(caleBackup)) {
        fs.mkdirSync(caleBackup, {recursive: true});
    }
    //in acest punct avem cai absolute in caleScss si caleCss

    let numeFisCss = path.basename(caleCss);
    if (fs.existsSync(caleCss)) {
        fs.copyFileSync(caleCss, path.join(obGlobal.folderBackup, "resurse/css", numeFisCss + (new Date()).getTime())) ;
    }
    let rez = sass.compile(caleScss, { "sourceMap": true });
    fs.writeFileSync(caleCss, rez.css);
    //console.log("Compilare SCSS", rez);
}

// Intervalul de timp T în minute
const TIMP = 10; // de exemplu, 10 minute

// Verificam si stergem fisierele vechi din folderul de backup la fiecare minut
setInterval(() => {
    let caleBackup = path.join(obGlobal.folderBackup, "resurse/css");
    stergeFisiereVechi(caleBackup, TIMP);
}, 60000);

// Funcția pentru ștergerea fișierelor vechi
function stergeFisiereVechi(caleBackup, TIMP) {
    fs.readdir(caleBackup, (err, files) => {
        if (err) {
            console.log(err);
        } else {
            files.forEach(file => {
                let filePath = path.join(caleBackup, file);
                // cu ajutorul fs.stat() aflam dif info despre fisiere (rezulta un obiect cu propr. mtime (timpul la care e conceput fisierul))
                fs.stat(filePath, (err, stats) => {
                    if (err) throw err;
    
                    let now = new Date().getTime();
                    let endTime = new Date(stats.mtime).getTime() + TIMP * 60 * 1000; // timp minute în milisecunde
    
                    if (now > endTime) {
                        //remove a file from the filesystem
                        fs.unlink(filePath, (err) => {
                            if (err) throw err;
                            console.log(`Fisierul ${file} a fost sters.`);
                        });
                    }
                });
            });
        }        
    });
}

vFisiere = fs.readdirSync(obGlobal.folderScss);
for (let numeFis of vFisiere) {
    if (path.extname(numeFis) == ".scss") {
        compileazaScss(numeFis);
    }
}

fs.watch(obGlobal.folderScss, function (eveniment, numeFis) {
    // console.log(eveniment, numeFis);
    if (eveniment == "change" || eveniment == "rename") {
        let caleCompleta = path.join(obGlobal.folderScss, numeFis);
        if (fs.existsSync(caleCompleta)) {
            compileazaScss(caleCompleta);
        }
    }
})

function genereazaNumar() {
    var puterileLuiDoi = [2, 4, 8, 16];
    var indexAleator = Math.floor(Math.random() * puterileLuiDoi.length);
    console.log("din fct genereaza nr " + puterileLuiDoi[indexAleator]);
    return puterileLuiDoi[indexAleator];
}

function getImagesWithEvenIndex(nrImaginiSelectate) {
    var imaginiGalAnim = obGlobal.obToateImagGalAnim.imagini;
    var result = [];
    for (let i = 0, j = 1; i < nrImaginiSelectate; i++, j = j + 2) {
        //dorim imaginile din galerie de pe pozitii nr. pare
        // avem grija ca indexul unui array incepe de la 0 (adica prima imagine din galerie)
        if (imaginiGalAnim[j]) {
            result.push(imaginiGalAnim[j]);
        }

    }
    return result;
}



app.listen(8080);
console.log("Serverul a pornit");